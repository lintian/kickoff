Kickoff - A Next-Generation Job Scheduler for Debian
======================================================

This repository is the home of the job scheduler that powers the
Lintian web site.

Questions or Problems?
======================

Please contact the
[Lintian Maintainers](https://qa.debian.org/developer.php?email=lintian-maint%40debian.org)
for additional needs.
