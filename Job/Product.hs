-- Copyright © 2021 Felix Lechner
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE DeriveGeneric #-}

module Job.Product where

import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           Data.Time.Clock                ( UTCTime )
import           GHC.Generics                   ( Generic )

import           Lintian.Results

data Product = Product
  { exit_status      :: Int
  , processing_end   :: UTCTime
  , processing_start :: UTCTime
  , stderr           :: String
  , stdout           :: Maybe Results
  }
  deriving (Show, Generic)

instance FromJSON Product
instance ToJSON Product
