-- Copyright © 2021 Felix Lechner
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE DeriveGeneric #-}

module Job.Task where

import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           Data.Time.Clock                ( UTCTime )
import           GHC.Generics                   ( Generic )

import           Job.Product
import           Job.Tool

data Task = Task
  { creation_time  :: UTCTime
  , inputs         :: [String]
  , name           :: String
  , port           :: String
  , product        :: Maybe Product
  , release        :: String
  , source_name    :: String
  , source_version :: String
  , tool           :: Maybe Tool
  , tool_name      :: String
  }
  deriving (Show, Generic)

instance FromJSON Task
instance ToJSON Task
