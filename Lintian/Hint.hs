-- Copyright © 2021 Felix Lechner
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE DeriveGeneric #-}

module Lintian.Hint where

import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           GHC.Generics                   ( Generic )

import           Lintian.ItemPointer
import           Lintian.Mask
import           Lintian.Override

data Hint = Hint
  { masks             :: Maybe [Mask]
  , note              :: Maybe String
  , override          :: Maybe Override
  , pointer           :: Maybe ItemPointer
  , tag               :: String
  , visibility        :: String
  }
  deriving (Show, Generic)

instance FromJSON Hint
instance ToJSON Hint
